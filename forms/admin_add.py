# coding: utf-8

from flask_wtf import Form
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired

class AddInviteForm(Form) :
	description = StringField(u'Описание для приглашенного', validators=[DataRequired()])
	activation_cnt = IntegerField(u'Количество активаций', validators=[DataRequired()])
	invite_id = StringField(u'Уникальный идентификатор приглашения', validators=[DataRequired()])