# coding: utf-8

from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import Email, DataRequired


class InviteForm(Form) :
	email = StringField(u'Ваш e-mail', validators=[DataRequired(), Email()])
