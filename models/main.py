from google.appengine.ext import db

class Invites(db.Model):
	invite_id = db.StringProperty(required=True)
	hits = db.IntegerProperty(default=0)
	description = db.StringProperty(default='')
	created_date = db.DateTimeProperty(auto_now_add=True)
	activation_cnt = db.IntegerProperty(default=0)
	subscription_cnt = db.IntegerProperty(default=0)


class UserInvites(db.Model):
	email  = db.StringProperty(required=True)
	domain = db.StringProperty(required=True)
	invite = db.ReferenceProperty(Invites, collection_name='user_data')


class InviteEntity(object):

	def __init__(self, invite_id):
		self.invite_id = invite_id

	@staticmethod
	def _coerce_domain(email):
		return email.split('@')[1]

	@property
	def invite_data(self):
		return Invites.all().filter('invite_id = ', self.invite_id).get()

	@property
	def exists(self):
		return True if self.invite_data else False

	@property
	def description(self):
		return self.invite_data.description

	def check_activation(self):
		if self.invite_data.activation_cnt == 0:
			return True
		if self.invite_data.subscription_cnt < self.invite_data.activation_cnt:
			return True
		return False

	def save(self, email):
		new_inv = self.invite_data
		new_inv.subscription_cnt += 1
		new_inv.put()

		UserInvites(invite=self.invite_data,
		            email=email,
		            domain=self._coerce_domain(email)).put()
		return self

	def add(self, description, activation_cnt=0):
		Invites(invite_id=self.invite_id,
		        description=description,
		        activation_cnt=activation_cnt).put()

	def add_hits(self):
		new_inv = self.invite_data
		new_inv.hits += 1
		new_inv.put()

	@property
	def can_invites(self):
		return True

class InviteCommands(object):
	pass