# coding: utf-8

from flask import Flask, render_template, make_response, request
from forms.invite import InviteForm
from models.main import InviteEntity

from blueprints.admin import admin_module

app = Flask(__name__)
app.config.from_object('config')
app.register_blueprint(admin_module)


@app.route('/')
def index() :
	return render_template('index.html')

@app.route('/invite/<invite_id>', methods=['GET', 'POST'])
def invite(invite_id) :
	inv = InviteEntity(invite_id)

	msg = {
			'unknown' : u'Упс. Но данного приглашения не существует',
			'successful' : u'Спасибо, что подписались на нас!',
			'inactive' : u'Нам очень жаль, но по данной '
			             u'ссылке активация приглашения невозможна.',
			'exceed' : u'Нам очень жаль, но количество активаций '
		                u'по ссылке превышено.',
			'thanks' : u'Спасибо, что подписались на нас!'
	}

	if not inv.exists:
		return render_template('invite.html', msg=msg['unknown'])
	inv.add_hits()

	subscribed = request.cookies.get('subscribed', False)
	if subscribed:
		return render_template('invite.html', msg=msg['successful'])

	if not inv.can_invites:
		return render_template('invite.html', msg=msg['inactive'])

	if not inv.check_activation():
		return render_template('invite.html', msg=msg['exceed'])
	form = InviteForm()

	if form.validate_on_submit():
		inv.save(form.email.data)
		resp = make_response(render_template('invite.html', msg=msg['thanks']))
		resp.set_cookie('subscribed', '1')
		return resp

	return render_template('invite.html',
	                       form=form,
	                       subscribed=subscribed,
	                       description=unicode(inv.description))


if __name__ == '__main__' :
	app.run(debug=True)
