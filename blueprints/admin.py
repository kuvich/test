# coding: utf-8

from collections import Counter
from functools import wraps

from google.appengine.api import users

from flask import Blueprint, render_template, make_response, redirect, jsonify
from models.main import InviteEntity, Invites, UserInvites
from forms.admin_add import AddInviteForm

admin_module = Blueprint('admin_module', __name__)

user = users.get_current_user()

def login_required(func):
	@wraps(func)
	def decorated_view(*args, **kwargs):
		if not users.is_current_user_admin():
			return 'access denied'
		return func(*args, **kwargs)
	return decorated_view


@login_required
@admin_module.route('/admin')
def index_admin():
	invite_lst = Invites.all()
	return render_template('admin_list.html', invite_lst=invite_lst)

@login_required
@admin_module.route('/admin/order/<order_type>')
def order_invites(order_type):
	invite_lst = Invites.all().order(order_type)
	return render_template('admin_list.html', invite_lst=invite_lst)

@login_required
@admin_module.route('/admin/add', methods=['GET', 'POST'])
def add_invites():
	form = AddInviteForm()

	if form.is_submitted():
		inv = InviteEntity(form.invite_id.data)
		inv.add(activation_cnt=form.activation_cnt.data,
				description=form.description.data)
		return redirect('/admin')
	return render_template('admin_add.html', form=form)

@login_required
@admin_module.route('/admin/stats')
def stats_invites():
	users = UserInvites.all()
	domain_lst =  [u.domain for u in users]
	stats = Counter(domain_lst)
	return render_template('admin_stats.html', stats=stats)

@login_required
@admin_module.route('/admin/export')
def export_invites():
	users = UserInvites.all()
	csv = reduce(lambda a, x: a + '\n' + x, [u.email for u in users])
	response = make_response(csv)
	response.headers["Content-Disposition"] = "attachment; filename=users.txt"
	return response
